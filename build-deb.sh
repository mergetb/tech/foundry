#!/bin/bash

mkdir -p build

rm -f build/foundry*.build*
rm -f build/foundry*.change
rm -f build/foundry*.deb

debuild -e V=1 -e prefix=/usr -e arch=amd64 $DEBUILD_ARGS -aamd64 -i -us -uc -b

mv ../foundry*.build* build/
mv ../foundry*.changes build/
mv ../foundry*.deb build/
