package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/alecthomas/chroma/quick"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"github.com/mergetb/yaml"
	api "gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/foundry/pkg"
)

var endpoint, cert string
var verify bool

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "foundry",
		Short: "manage foundry machine configs",
	}
	root.PersistentFlags().StringVar(
		&endpoint, "endpoint", "localhost:27000", "foundry endpoint")
	root.PersistentFlags().StringVar(
		&cert, "cert", "", "manage TLS cert")
	root.PersistentFlags().BoolVar(
		&verify, "verify", false, "verify servers tls cert",
	)

	version := &cobra.Command{
		Use:   "version",
		Short: "print version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Print(foundry.Version)
		},
	}
	root.AddCommand(version)

	var autocomplete = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			root.GenBashCompletion(os.Stdout)
		},
	}
	root.AddCommand(autocomplete)

	set := &cobra.Command{
		Use:   "set <config.yaml>",
		Short: "set machine configs",
		Long: "set machine configs, yaml file may contain multiple configs as\n" +
			"individual documents",
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			set(args[0])
		},
	}
	root.AddCommand(set)

	get := &cobra.Command{
		Use:   "get <machine-id> [<macine-id> ...]",
		Short: "get a machine's config",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			get(args)
		},
	}
	root.AddCommand(get)

	del := &cobra.Command{
		Use:   "delete <machine-id> [<machine-id> ...]",
		Short: "delete a machine's config",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			del(args)
		},
	}
	root.AddCommand(del)

	list := &cobra.Command{
		Use:   "list <machine-id>",
		Short: "list all configs",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			list()
		},
	}
	root.AddCommand(list)

	root.Execute()

}

func set(filename string) {

	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		log.Fatal(err)
	}

	var configs []*api.MachineConfig
	dc := yaml.NewDecoder(f)

	for err == nil {

		config := &api.MachineConfig{}
		err = dc.Decode(config)
		if err == nil {
			configs = append(configs, config)
		}

	}

	if err != io.EOF {
		log.Fatal(err)
	}

	withFoundry(func(ctx context.Context, cli api.ManageClient) error {

		_, err = cli.Set(ctx, &api.SetRequest{Configs: configs})
		if err != nil {
			log.Fatal(err)
		}

		return nil

	})

}

func get(ids []string) {

	withFoundry(func(ctx context.Context, cli api.ManageClient) error {

		configs, err := cli.Get(ctx, &api.GetRequest{Ids: ids})
		if err != nil {
			log.Fatal(err)
		}

		out, err := yaml.Marshal(configs)
		if err != nil {
			log.Fatal(err)
		}

		quick.Highlight(os.Stdout, string(out), "yaml", "terminal256", "pygments")

		return nil

	})

}

func del(ids []string) {

	withFoundry(func(ctx context.Context, cli api.ManageClient) error {

		_, err := cli.Delete(ctx, &api.DeleteRequest{Ids: ids})
		if err != nil {
			log.Fatal(err)
		}

		return nil

	})

}

func list() {
	withFoundry(func(ctx context.Context, cli api.ManageClient) error {

		lst, err := cli.List(ctx, &api.ListRequest{})
		if err != nil {
			log.Fatal(err)
		}

		out, err := yaml.Marshal(lst.Result)
		if err != nil {
			log.Fatal(err)
		}

		quick.Highlight(os.Stdout, string(out), "yaml", "terminal256", "pygments")

		return nil

	})
}

func connect() (*grpc.ClientConn, api.ManageClient) {

	if cert != "" {

		var creds credentials.TransportCredentials
		var err error
		if verify {
			creds, err = credentials.NewClientTLSFromFile(cert, endpoint)
		} else {
			creds, err = tlsConfig(endpoint)
		}
		if err != nil {
			log.Fatalf("failed to read cert file: %v", err)
		}

		conn, err := grpc.Dial(endpoint, grpc.WithTransportCredentials(creds))
		if err != nil {
			log.Fatalf("foundry connection failed: %v", err)
		}
		client := api.NewManageClient(conn)

		return conn, client

	} else {

		conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("foundry connection failed: %v", err)
		}
		client := api.NewManageClient(conn)

		return conn, client

	}

}

func withFoundry(f func(context.Context, api.ManageClient) error) error {

	conn, cli := connect()
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	return f(ctx, cli)

}

func tlsConfig(endpoint string) (credentials.TransportCredentials, error) {

	b, err := ioutil.ReadFile(cert)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	return credentials.NewTLS(&tls.Config{
		ServerName:         endpoint,
		RootCAs:            cp,
		InsecureSkipVerify: true,
	}), nil

}
