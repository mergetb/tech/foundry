package main

import (
	"context"
	"flag"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	api "gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/foundry/pkg"
)

type manager struct{}

var (
	noauth = flag.Bool(
		"no-auth", false, "do not authenticate management connections")
	mEndpoint  = flag.String("manage-endpoint", "0.0.0.0:27000", "listening endpoint")
	cert       = flag.String("cert", "/etc/foundry/manage.pem", "forge TLS cert")
	key        = flag.String("key", "/etc/foundry/manage-key.pem", "forge TLS key")
	maxMsgSize = flag.Int("maxsize", 4, "grpc and etcd max message size (MB)")
)

func runManager() {

	s := &manager{}

	fields := log.Fields{
		"noauth":           *noauth,
		"manage endpoint":  *mEndpoint,
		"cert":             *cert,
		"key":              *key,
		"max message size": *maxMsgSize,
	}

	if *maxMsgSize > 0 {
		foundry.MaxMessageSize = *maxMsgSize * 1024 * 1024
	}

	opts := foundry.GRPCServerOptions()

	var gs *grpc.Server
	if !*noauth {
		creds, err := credentials.NewServerTLSFromFile(*cert, *key)
		if err != nil {
			log.Fatal(err)
		}
		opts = append(opts, grpc.Creds(creds))
	}

	gs = grpc.NewServer(opts...)

	api.RegisterManageServer(gs, s)
	l, err := net.Listen("tcp", *mEndpoint)
	if err != nil {
		log.Fatal(err)
	}
	log.WithFields(fields).Infof("listening on %s", *mEndpoint)
	gs.Serve(l)

}

func (s *manager) Set(
	ctx context.Context, rq *api.SetRequest,
) (*api.SetResponse, error) {

	return &api.SetResponse{}, foundry.SetConfigs(rq.Configs)

}

func (s *manager) Get(
	ctx context.Context, rq *api.GetRequest,
) (*api.GetResponse, error) {

	configs, err := foundry.GetConfigs(rq.Ids)
	if err != nil {
		return nil, err
	}

	return &api.GetResponse{Configs: configs}, nil

}

func (s *manager) Delete(
	ctx context.Context, rq *api.DeleteRequest,
) (*api.DeleteResponse, error) {

	return &api.DeleteResponse{}, foundry.DeleteConfigs(rq.Ids)

}

func (s *manager) List(
	ctx context.Context, rq *api.ListRequest,
) (*api.ListResponse, error) {

	result, err := foundry.ListConfigs()
	if err != nil {
		return nil, err
	}

	return &api.ListResponse{Result: result}, nil

}
