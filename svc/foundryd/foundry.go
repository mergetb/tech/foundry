package main

import (
	"context"
	"flag"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	api "gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/foundry/pkg"
)

var (
	fEndpoint = flag.String("forge-endpoint", "0.0.0.0:47001", "listening endpoint")
)

type forge struct{}

func runFoundry() {

	s := &forge{}
	gs := grpc.NewServer()

	api.RegisterForgeServer(gs, s)
	l, err := net.Listen("tcp", *fEndpoint)
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("listening on %s", *fEndpoint)
	gs.Serve(l)

}

func (s *forge) Forge(
	ctx context.Context, rq *api.ForgeRequest,
) (*api.ForgeResponse, error) {

	log.WithFields(log.Fields{"ids": rq.Ids}).Info("forge")

	config, err := foundry.Prepare(rq)
	if err != nil {
		return nil, err
	}

	return &api.ForgeResponse{Config: config}, nil

}
