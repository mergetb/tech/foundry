package foundry

import (
	"fmt"
	"runtime"

	log "github.com/sirupsen/logrus"
)

func InitLogging(logname string) {

	log.SetLevel(log.DebugLevel)

}

// Error creates an error, and logs it righteously
func Error(message string) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).Error(message)

	return fmt.Errorf(message)

}

// Encapsulate err in a structured log and return an abstracted high-level
// error with message as the payload
func ErrorE(message string, err error) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"error":  err,
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).Error(message)

	return fmt.Errorf(message)

}

// Encapsulate fields in a structured log and return an abstracted high-level
// error with message as the payload
func ErrorF(message string, fields log.Fields) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).WithFields(fields).Error(message)

	return fmt.Errorf(message)

}

// Encapsulate fields and err in a structured log and return an abstracted
// high-level error with message as the payload
func ErrorEF(message string, err error, fields log.Fields) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"error":  err,
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).WithFields(fields).Error(message)

	return fmt.Errorf(message)

}
