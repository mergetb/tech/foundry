package foundry

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
)

// all obejcts that go to and fro from the database implement this interface
type Object interface {
	Key() string
	GetVersion() int64
	SetVersion(int64)
	Value() interface{}
}

var NotFound error = fmt.Errorf("not found")

func Read(obj Object) error {

	err, n := ReadObjects([]Object{obj})
	if err != nil {
		return err
	}
	if n == 0 {
		return NotFound
	}

	return nil

}

func ReadNew(obj Object) error {

	err := Read(obj)
	if err != nil && err != NotFound {
		return err
	}

	return nil

}

func ReadObjects(objs []Object) (error, int) {

	var ops []etcd.Op
	omap := make(map[string]Object)

	for _, o := range objs {
		omap[o.Key()] = o
		ops = append(ops, etcd.OpGet(o.Key()))
	}

	n := 0
	err := withEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("")
		}

		for _, r := range resp.Responses {
			rr := r.GetResponseRange()
			if rr == nil {
				continue
			}

			for _, kv := range rr.Kvs {
				n += 1
				o := omap[string(kv.Key)]

				switch t := o.Value().(type) {
				case *string:
					*t = string(kv.Value)
				default:
					err := json.Unmarshal(kv.Value, o.Value())
					if err != nil {
						return err
					}
					o.SetVersion(kv.Version)
				}
			}
		}

		return nil

	})

	return err, n

}

func Write(obj Object, opts ...etcd.OpOption) error {

	return WriteObjects([]Object{obj}, opts...)

}

func WriteObjects(objs []Object, opts ...etcd.OpOption) error {

	var ops []etcd.Op
	var ifs []etcd.Cmp

	for _, obj := range objs {

		var value string
		switch t := obj.Value().(type) {
		case *string:
			value = *t
		default:
			buf, err := json.MarshalIndent(obj.Value(), "", "  ")
			if err != nil {
				return err
			}
			value = string(buf)
		}

		ops = append(ops, etcd.OpPut(obj.Key(), value, opts...))
		/*
			ifs = append(ifs,
				etcd.Compare(etcd.Version(obj.Key()), "=", obj.GetVersion()))
		*/

	}

	return withEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		if kvc == nil {
			log.Error("failed to create etcd client")
			return fmt.Errorf("failed to create etcd client")
		}

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := kvc.Txn(ctx).If(ifs...).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("state has changed since read")
		}

		for _, o := range objs {
			o.SetVersion(o.GetVersion() + 1)
		}

		return nil

	})

}

func DeleteObjects(objs []Object) error {

	var ops []etcd.Op

	for _, obj := range objs {

		ops = append(ops, etcd.OpDelete(obj.Key()))

	}

	return withEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("delete objects failed")
		}

		return nil

	})

}

func Delete(obj Object) error {

	return DeleteObjects([]Object{obj})

}

type ObjectTx struct {
	Put    []Object
	Delete []Object
}

func RunObjectTx(otx ObjectTx) error {

	var ops []etcd.Op
	for _, x := range otx.Put {

		var value string
		switch t := x.Value().(type) {
		case *string:
			value = *t
		default:
			buf, err := json.MarshalIndent(x.Value(), "", "  ")
			if err != nil {
				return err
			}
			value = string(buf)
		}

		ops = append(ops, etcd.OpPut(x.Key(), string(value)))
	}
	for _, x := range otx.Delete {
		ops = append(ops, etcd.OpDelete(x.Key()))
	}

	return withEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("run object txn failed")
		}

		return nil

	})

}

const (
	TxnFailedPrefix = "txn failed"
)

func TxnFailed(message string) error {
	err := fmt.Errorf("%s: %s", TxnFailedPrefix, message)
	log.Error(err)
	return err
}

func IsTxnFailed(err error) bool {
	return strings.HasPrefix(err.Error(), TxnFailedPrefix)
}
