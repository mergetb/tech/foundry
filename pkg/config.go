package foundry

import (
	"io/ioutil"

	"github.com/mergetb/yaml"
	log "github.com/sirupsen/logrus"
)

type Config struct {
	ManageEndpoint string     `yaml:"manage_endpoint"`
	EtcdEndpoint   string     `yaml:"etcd_endpoint"`
	EtcdTls        *TlsConfig `yaml:"etcd_tls"`
}

type TlsConfig struct {
	CA   string `key:"ca"`
	Cert string `yaml:"cert"`
	Key  string `yaml:"key"`
}

var config *Config

func ServerConfig() *Config {

	if config != nil {
		return config
	}
	return LoadConfig()

}

func LoadConfig() *Config {

	buf, err := ioutil.ReadFile("/etc/foundry/config.yml")
	if err != nil {
		log.WithError(err).Fatal("could not read config")
	}

	c := &Config{}
	err = yaml.Unmarshal(buf, c)
	if err != nil {
		log.WithError(err).Fatal("could not parse config")
	}

	config = c
	return config

}
