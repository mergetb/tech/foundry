package forge

import (
	"fmt"
	"os/exec"

	"github.com/cbednarski/hostess/hostess"
	log "github.com/sirupsen/logrus"

	api "gitlab.com/mergetb/tech/foundry/api"
)

func ForgeHostname(hostname string, use_systemd bool) error {

	log.WithFields(log.Fields{"hostname": hostname}).Info("setting hostname")

	cmd := exec.Command("hostnamectl", "set-hostname", hostname)
	if !use_systemd {
		cmd = exec.Command("hostname", hostname)
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		return ErrorEF("failed to set hostname", err, log.Fields{
			"hostname": hostname,
			"output":   string(out),
		})
	}

	return nil

}

func ForgeHosts(hosts []*api.Host) error {

	if hosts == nil || len(hosts) == 0 {
		return nil
	}

	hostfile, errs := hostess.LoadHostfile()
	if len(errs) != 0 {
		return ErrorE("failed to load hostfile", fmt.Errorf("%+v", errs))
	}

	for _, host := range hosts {
		name := host.Name
		ipv4 := host.Ipv4
		ipv6 := host.Ipv6

		log.WithFields(log.Fields{"name": name, "ipv4": ipv4, "ipv6": ipv6}).Info("setting /etc/hosts")

		if ipv4 != "" {
			newHostname, err := hostess.NewHostname(name, ipv4, true)
			if err != nil {
				return ErrorEF("failed to create hostname", err, log.Fields{
					"name": name,
					"ipv4": ipv4,
				})
			}

			hostfile.Hosts.Add(newHostname)
		}

		if ipv6 != "" {
			newHostname, err := hostess.NewHostname(name, ipv6, true)
			if err != nil {
				return ErrorEF("failed to create hostname", err, log.Fields{
					"name": name,
					"ipv6": ipv6,
				})
			}

			hostfile.Hosts.Add(newHostname)
		}
	}

	err := hostfile.Save()
	if err != nil {
		return ErrorE("failed to save namehostfile", err)
	}

	return nil
}
