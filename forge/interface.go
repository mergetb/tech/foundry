package forge

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"text/template"

	log "github.com/sirupsen/logrus"

	api "gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/rtnl"
)

const primaryInterfaceTemplate = `
[Match]
Name={{ .Name }}

[Network]
DHCP=yes
LLMNR=no
DNSSEC=no
LLDP=yes
EmitLLDP=yes

[DHCP]
ClientIdentifier=mac
UseDomains=yes
UseMTU=yes
RouteMetric=512
`

const primarySubInterfaceParentTemplate = `
[Match]
Name={{ .Name }}

[Network]
DHCP=yes
LLMNR=no
DNSSEC=no
LLDP=yes
EmitLLDP=yes
VLAN=infranet

[DHCP]
ClientIdentifier=mac
UseDomains=yes
UseMTU=yes
UseDNS=no
RouteMetric=1024
`

type SubInterfaceNetdevParams struct {
	Vid int
}

const primarySubInterfaceNetdevTemplate = `
[NetDev]
Name=infranet
Kind=vlan

[VLAN]
Id={{ .Vid }}
`

const primarySubInterfaceNetworkTemplate = `
[Match]
Name=infranet

[Network]
DHCP=yes
LLMNR=no
DNSSEC=no
LLDP=yes
EmitLLDP=yes

[DHCP]
ClientIdentifier=mac
UseDomains=yes
UseMTU=yes
RouteMetric=512
`

type InterfaceTemplateArgs struct {
	Name string
}

func findDevice(macstr string) (string, error) {
	mac, err := net.ParseMAC(macstr)
	if err != nil {
		return "", fmt.Errorf("parse MAC %s: %v", macstr, err)
	}

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return "", fmt.Errorf("rtnl open: %v", err)
	}
	defer rtx.Close()

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return "", fmt.Errorf("rtnl readlinks: %v", err)
	}

	for _, link := range links {
		if bytes.Equal(mac, link.Info.Address) {
			return link.Info.Name, nil
		}
	}

	return "", fmt.Errorf("could not find link with MAC address %s", macstr)
}

func findDeviceWithFallback(mac, fallback string) string {
	name, err := findDevice(mac)

	if err != nil {
		log.Infof("%v", err)
		return fallback
	}

	return name
}

func linkByMac(rtx *rtnl.Context, mac string) (*rtnl.Link, error) {
	addr, err := net.ParseMAC(mac)
	if err != nil {
		return nil, fmt.Errorf("parse MAC %s: %v", mac, err)
	}

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return nil, fmt.Errorf("read links: %v", err)
	}

	for _, link := range links {
		// look for physical links
		if link.Info.Type() == rtnl.PhysicalType && bytes.Equal(addr, link.Info.Address) {
			return link, nil
		}
	}

	return nil, fmt.Errorf("cannot find link with MAC %s", mac)
}

func linkByMacOrName(rtx *rtnl.Context, name, mac string) (*rtnl.Link, error) {
	if mac != "" {
		return linkByMac(rtx, mac)
	}

	if name != "" {
		return rtnl.GetLink(rtx, name)
	}

	return nil, fmt.Errorf("no mac or name for port")
}

func ForgePrimaryInterface(use_systemd bool) error {

	var err error
	infradev := ""
	infravid := 0

	cmdline, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}

	args := strings.Fields(string(cmdline))
	for _, x := range args {
		parts := strings.Split(x, "=")
		if len(parts) == 2 {
			if parts[0] == "inframac" {
				infradev, err = findDevice(parts[1])
				if err != nil {
					return fmt.Errorf("find infranet device: %v", err)
				}
			}

			if parts[0] == "infravid" {
				infravid, err = strconv.Atoi(parts[1])
				if err != nil {
					return fmt.Errorf("atoi %s: %v", parts[1], err)
				}
			}
		}
	}

	if infradev == "" {
		return fmt.Errorf("primary interface not found. 'inframac=<mac>' expected on kernel cmdline")
	}

	// default to systemd versions
	forgePrimary := forgePrimaryInterface
	forgePrimarySub := forgePrimarySubInterface

	if !use_systemd {
		forgePrimary = forgePrimaryInterfaceNoSystemd
		forgePrimarySub = forgePrimarySubInterfaceNoSystemd
	}

	if infravid > 0 {
		return forgePrimarySub(infradev, infravid)
	}

	return forgePrimary(infradev)

}

func forgePrimarySubInterface(name string, vid int) error {

	log.WithFields(log.Fields{
		"ifx": name,
		"vid": vid,
	}).Info("forging vlan subinterface as primary")

	// parent interface

	tmpl, err := template.New("ifx").Parse(primarySubInterfaceParentTemplate)
	if err != nil {
		return fmt.Errorf("failed to parse parent template: %v", err)
	}

	filename := fmt.Sprintf("/etc/systemd/network/%s.network", name)
	f, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", filename, err)
	}

	err = tmpl.Execute(f, InterfaceTemplateArgs{
		Name: name,
	})

	if err != nil {
		return fmt.Errorf(
			"failed to execute primary interface template: %v", err)
	}

	// subinterface netdev

	tmpl, err = template.New("ifx").Parse(primarySubInterfaceNetdevTemplate)
	if err != nil {
		return fmt.Errorf("failed to parse netdev template: %v", err)
	}

	filename = "/etc/systemd/network/infranet.netdev"
	f, err = os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", filename, err)
	}

	err = tmpl.Execute(f, SubInterfaceNetdevParams{
		Vid: vid,
	})

	if err != nil {
		return fmt.Errorf(
			"failed to execute primary subinterface netdev template: %v", err)
	}

	// subinterface network

	err = ioutil.WriteFile(
		"/etc/systemd/network/infranet.network",
		[]byte(primarySubInterfaceNetworkTemplate),
		0644,
	)
	if err != nil {
		return fmt.Errorf(
			"failed to write primary subinterface network config: %v", err)
	}

	return nil

}

func forgePrimarySubInterfaceNoSystemd(name string, vid int) error {

	vlan_name := "infranet"

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	parent, err := linkByMacOrName(rtx, name, "")
	if err != nil {
		return fmt.Errorf("vlan get parent %s: %v", name, err)
	}

	lnk := rtnl.NewLink()
	lnk.Info.Name = vlan_name
	lnk.Info.Vlan = &rtnl.Vlan{
		Id:   uint16(vid),
		Link: uint32(parent.Msg.Index),
	}

	err = lnk.Present(rtx)
	if err != nil {
		return fmt.Errorf("vlan present: %v", err)
	}

	err = parent.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan parent up: %v", err)
	}

	err = lnk.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan up: %v", err)
	}

	exec.Command("pkill", "udhcpc").CombinedOutput()
	// ignore pkill errors

	_, err = exec.Command("/sbin/udhcpc", "-b", "-i", vlan_name, "-O", "mtu", "-p", fmt.Sprintf("/var/run/udhcpc.%s.pid", vlan_name)).CombinedOutput()
	if err != nil {
		return fmt.Errorf("udhcpc: %v", err)
	}

	return nil
}

func forgePrimaryInterface(name string) error {

	log.WithFields(log.Fields{
		"ifx": name,
	}).Info("forging primary interface")

	tmpl, err := template.New("ifx").Parse(primaryInterfaceTemplate)
	if err != nil {
		return fmt.Errorf("failed to parse template: %v", err)
	}

	filename := fmt.Sprintf("/etc/systemd/network/%s.network", name)
	f, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", filename, err)
	}

	err = tmpl.Execute(f, InterfaceTemplateArgs{
		Name: name,
	})

	if err != nil {
		return fmt.Errorf(
			"failed to execute primary interface template: %v", err)
	}

	return nil

}

func forgePrimaryInterfaceNoSystemd(name string) error {

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	lnk, err := linkByMacOrName(rtx, name, "")
	if err != nil {
		return fmt.Errorf("port get name %s: %v", name, err)
	}

	err = lnk.Up(rtx)
	if err != nil {
		return fmt.Errorf("link up %s: %v", name, err)
	}

	exec.Command("pkill", "udhcpc").CombinedOutput()
	// ignore pkill errors

	_, err = exec.Command("/sbin/udhcpc", "-b", "-i", name, "-p", fmt.Sprintf("/var/run/udhcpc.%s.pid", name)).CombinedOutput()
	if err != nil {
		return fmt.Errorf("udhcpc: %v", err)
	}

	return nil
}

func ForgeInterfaces(interfaces []*api.Interface, use_systemd bool) error {

	// default to systemd forging
	vxlan := ForgeVxlan
	vlan := ForgeVlan
	phy := ForgePhy

	if !use_systemd {
		vxlan = ForgeVxlanNoSystemd
		vlan = ForgeVlanNoSystemd
		phy = ForgePhyNoSystemd
	}

	for _, ifx := range interfaces {

		if ifx.Vxlan != nil {
			err := vxlan(ifx)
			if err != nil {
				return err
			}
		} else if ifx.Vlan != nil {
			err := vlan(ifx)
			if err != nil {
				return err
			}
		} else {
			err := phy(ifx)
			if err != nil {
				return err
			}
		}
	}

	return nil

}

func ForgeVlan(ifx *api.Interface) error {

	ifx.Name = findDeviceWithFallback(ifx.Mac, ifx.Name)

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"vid":   ifx.Vlan.Vid,
		"dhcp":  ifx.Vlan.Dhcp,
	}
	log.WithFields(fields).Info("add vlan")

	// read in existing network file if it exists
	cfg, err := Read(fmt.Sprintf("%s.network", ifx.Name))
	if err != nil {
		cfg = make(SystemdConfig)
		cfg.MatchName(ifx.Name)
	}
	cfg.SetVlan(ifx.Name, int(ifx.Vlan.Vid))
	cfg.Write(ifx.Name, "network")

	cfg = make(SystemdConfig)
	netdev := fmt.Sprintf("%s.%d", ifx.Name, int(ifx.Vlan.Vid))
	cfg.Netdev(netdev, "vlan")
	cfg.NetdevVlan(int(ifx.Vlan.Vid))
	cfg.Write(netdev, "netdev")

	cfg = make(SystemdConfig)
	cfg.MatchName(netdev)

	for _, addr := range ifx.Addrs {
		if ValidateAddress(addr) {
			cfg.SetAddress(addr)
		}
	}

	if ifx.Vlan.Dhcp != nil {
		if ValidateDHCP(ifx.Vlan.Dhcp.Dhcp) {
			cfg.SetDHCP(ifx.Vlan.Dhcp.Dhcp)
			cfg.SetDHCPConfig(ifx.Vlan.Dhcp.UseDomains, ifx.Vlan.Dhcp.UseDns)
		} else {
			log.Errorf("not valid dhcp option: %s", ifx.Vlan.Dhcp)
			log.WithError(
				fmt.Errorf("Invalid dchp option"),
			).WithFields(log.Fields{
				"dhcp": ifx.Vlan.Dhcp,
			})
		}
	}
	cfg.Write(netdev, "network")

	return nil

}

func ForgeVlanNoSystemd(ifx *api.Interface) error {

	vid_suffix := fmt.Sprintf(".%d", ifx.Vlan.Vid)

	// there's a limitation of 16 characters for the interface name
	// shorten it the name if it's too long
	index := len(ifx.Name)
	if len(ifx.Name)+len(vid_suffix) > 16 {
		index = 16 - len(vid_suffix)
	}

	vlan_name := fmt.Sprintf("%s%s", ifx.Name[:index], vid_suffix)

	ifx.Name = findDeviceWithFallback(ifx.Mac, ifx.Name)

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"vid":   ifx.Vlan.Vid,
		"dhcp":  ifx.Vlan.Dhcp,
	}
	log.WithFields(fields).Info("add vlan no systemd")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	parent, err := linkByMacOrName(rtx, ifx.Name, ifx.Mac)
	if err != nil {
		return fmt.Errorf("port get %s/%s: %v", ifx.Name, ifx.Mac, err)
	}

	lnk := rtnl.NewLink()
	lnk.Info.Name = vlan_name
	lnk.Info.Vlan = &rtnl.Vlan{
		Id:   uint16(ifx.Vlan.Vid),
		Link: uint32(parent.Msg.Index),
	}

	err = lnk.Present(rtx)
	if err != nil {
		return fmt.Errorf("vlan present: %v", err)
	}

	for _, x := range ifx.Addrs {

		if ValidateAddress(x) {
			ip, ipnet, err := net.ParseCIDR(x)
			if err != nil {
				return fmt.Errorf("parse addr %s: %v", x, err)
			}

			addr := rtnl.NewAddress()
			addr.Info.Address = &net.IPNet{
				IP:   ip,
				Mask: ipnet.Mask,
			}

			err = lnk.AddAddr(rtx, addr)
			if err != nil {
				if !strings.Contains(err.Error(), "exists") {
					return fmt.Errorf("add addr %s: %v", x, err)
				}
			}
		}
	}

	if ifx.Mtu != 0 {
		err = lnk.SetMtu(rtx, int(ifx.Mtu))
		if err != nil {
			return fmt.Errorf("set mtu: %v", err)
		}
	}

	if ifx.Vlan.Dhcp != nil {
		log.Warnf("dhcp not supported without systemd")
	}

	err = parent.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan parent up: %v", err)
	}

	err = lnk.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan up: %v", err)
	}

	return nil
}

//TODO
func ForgeVxlan(ifx *api.Interface) error {

	return nil

}

//TODO
func ForgeVxlanNoSystemd(ifx *api.Interface) error {

	return nil

}

func ForgePhy(ifx *api.Interface) error {

	ifx.Name = findDeviceWithFallback(ifx.Mac, ifx.Name)

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"dhcp":  ifx.Dhcp,
	}
	log.WithFields(fields).Info("add phy")

	cfg := make(SystemdConfig)
	cfg.MatchName(ifx.Name)

	for _, addr := range ifx.Addrs {
		if ValidateAddress(addr) {
			cfg.SetAddress(addr)
		}
	}

	if ifx.Dhcp != nil {
		if ValidateDHCP(ifx.Dhcp.Dhcp) {
			cfg.SetDHCP(ifx.Dhcp.Dhcp)
			cfg.SetDHCPConfig(ifx.Dhcp.UseDomains, ifx.Dhcp.UseDns)
		} else {
			log.WithError(
				fmt.Errorf("Invalid dchp option"),
			).WithFields(log.Fields{
				"dhcp": ifx.Dhcp,
			})
		}
	}

	if int(ifx.Mtu) != 0 {
		cfg.SetMtu(int(ifx.Mtu))
	}

	return cfg.Write(ifx.Name, "network")

}

func ForgePhyNoSystemd(ifx *api.Interface) error {

	ifx.Name = findDeviceWithFallback(ifx.Mac, ifx.Name)

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"dhcp":  ifx.Dhcp,
	}
	log.WithFields(fields).Info("add phy no systemd")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	lnk, err := linkByMacOrName(rtx, ifx.Name, ifx.Mac)
	if err != nil {
		return fmt.Errorf("port get %s/%s: %v", ifx.Name, ifx.Mac, err)
	}

	for _, x := range ifx.Addrs {

		if ValidateAddress(x) {
			ip, ipnet, err := net.ParseCIDR(x)
			if err != nil {
				return fmt.Errorf("parse addr %s: %v", x, err)
			}

			addr := rtnl.NewAddress()
			addr.Info.Address = &net.IPNet{
				IP:   ip,
				Mask: ipnet.Mask,
			}

			err = lnk.AddAddr(rtx, addr)
			if err != nil {
				if !strings.Contains(err.Error(), "exists") {
					return fmt.Errorf("add addr %s: %v", x, err)
				}
			}
		}

	}

	if ifx.Mtu != 0 {
		err = lnk.SetMtu(rtx, int(ifx.Mtu))
		if err != nil {
			return err
		}
	}

	if ifx.Dhcp != nil {
		log.Printf("dhcp not supported without systemd")
	}

	err = lnk.Up(rtx)
	if err != nil {
		return err
	}

	return nil

}

func ValidateDHCP(dhcp string) bool {
	lower := strings.ToLower(dhcp)
	if lower == "yes" || lower == "no" || lower == "ipv4" || lower == "ipv6" {
		return true
	}
	return false
}

func ValidateAddress(addr string) bool {

	if addr == "" {
		return false
	}

	_, _, err := net.ParseCIDR(addr)
	if err != nil {

		log.WithError(err).WithFields(log.Fields{
			"address": addr,
		}).Warn("invalid address")

		return false

	}

	return true

}
