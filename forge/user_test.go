package forge

import (
	"os"
	"os/user"
	"reflect"
	"testing"
)

const ENV = "MERGE_TEST_ADD_CLEANUP_SSH_PRIVATE_KEYS"

// TestAddCleanupSshPrivateKeys tests adding/cleanup of ssh identity files
// If this test is run, it will delete any files ~/.ssh/id_mrg-[0-9]* and
// purge ~/.ssh/config from any mention of them
// An Environment ENV needs to be set for this test to run
func TestAddCleanupSshPrivateKeys(t *testing.T) {
	if os.Getenv(ENV) == "" {
		t.Skipf("Skipping TestAddCleanupSshPrivateKeys: if you want to run it: set env %v=true\n"+
			"If you have any files named .ssh/id_mrg-[0-9]* and/or correspoding IdentityFiles\n"+
			"in .ssh/config, running this test will remove them", ENV)
	}
	u, err := user.Current()
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = CleanupSshPrivateKeys(u.Username)
	if err != nil {
		t.Logf("error: %#v", err)
		t.Fatal(err)
	}
	added_ids, err := AddSshPrivateKeys(u.Username, []string{"private-key 1 xxx\n", "private-key 2 yyy\n"})
	if err != nil {
		t.Fatal(err)
	}
	keys, removed_ids, err := CleanupSshPrivateKeys(u.Username)
	if err != nil {
		t.Fatal(err)
	}
	if len(added_ids) != 2 || !reflect.DeepEqual(added_ids, removed_ids) {
		t.Logf("Installed and removed ids didn't match: %#v != %#v, files: %#v", added_ids, removed_ids, keys)
	}
}
