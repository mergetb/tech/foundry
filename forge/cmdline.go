package forge

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"regexp"
	"strings"
)

const (
	GRUB_STRING = "# FOUNDRY CMDLINE APPENDED TO ENTRIES: "
)

func ForgeCmdline(grub_cfg string) error {

	if grub_cfg == "" {
		return ForgeCmdlineDefault()
	}

	return ForgeCmdlineRawCfg(grub_cfg)

}

func ForgeCmdlineDefault() error {

	log.Printf("forging /etc/default/grub")

	orig, err := ioutil.ReadFile("/etc/default/grub")
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}
	cmdline := strings.TrimSuffix(string(buf), "\n")

	re := regexp.MustCompile(`GRUB_CMDLINE_LINUX=".*"`)

	new := re.ReplaceAll(orig, []byte(
		fmt.Sprintf("GRUB_CMDLINE_LINUX=\"%s\"", cmdline),
	))

	if string(orig) == string(new) {
		return nil
	}

	err = ioutil.WriteFile("/etc/default/grub", new, 0644)
	if err != nil {
		return fmt.Errorf("failed to write to /etc/default/grub: %v", err)
	}

	out, err := exec.Command("update-grub2").CombinedOutput()
	if err != nil {
		return fmt.Errorf("update grub failed: %s: %v", string(out), err)
	}

	return nil

}

func ForgeCmdlineRawCfg(grub_cfg string) error {

	log.Printf("forging %s", grub_cfg)

	orig, err := ioutil.ReadFile(grub_cfg)
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}
	cmdline := strings.TrimSuffix(string(buf), "\n")

	if strings.Contains(string(orig), GRUB_STRING) {
		// we have already written to grub, so just return
		return nil
	}

	// prepend the foundry written message to the configuration file,
	// so we don't append the cmdline multiple times
	orig = append([]byte(GRUB_STRING+cmdline+"\n"), orig...)

	re := regexp.MustCompile(`linux .*`)

	new := re.ReplaceAll(orig, []byte("${0} "+cmdline))

	if string(orig) == string(new) {
		return nil
	}

	err = ioutil.WriteFile(grub_cfg, new, 0644)
	if err != nil {
		return fmt.Errorf("failed to write to %s: %v", grub_cfg, err)
	}

	return nil

}
