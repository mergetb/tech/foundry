package forge

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	api "gitlab.com/mergetb/tech/foundry/api"
)

func validateMountInputs(mount *api.Mount) error {
	return nil
}

// AddMounts adds mounts to node
func AddMounts(mounts []*api.Mount) error {

	// create an fstab file representation in memory
	tab := &FSTabFile{}
	err := tab.FSTabParse()
	if err != nil {
		return err
	}

	// for each mount, check it is valid, and add to memory repr
	for _, mount := range mounts {

		err = validateMountInputs(mount)
		if err != nil {
			return err
		}

		err = tab.AddFSTabEntry(mount)
		if err != nil {
			return err
		}

	}

	// write back memory repr to fstab file
	err = tab.FSTabWrite()
	if err != nil {
		return err
	}

	// run mount to pick up the changes and mount the devices
	err = tab.RunMount()
	if err != nil {
		return err
	}

	return nil

}

//172.29.0.10:6789:/ /mnt ceph rw,relatime,name=admin,secret=,acl 0 0

// TODO: Add struct for options to allow flag unions (rw|ro, a|sync)

// FSTab is the internal fstab format
type FSTab struct {
	Device    string
	Directory string
	Type      string
	Options   []string
	Dump      int
	Fsck      int
}

// FSTabFile wraps all the FSTab entries in /etc/fstab
type FSTabFile struct {
	// Entries is a linux user to FsTab Entry, the user key, tells us how to chown the directory
	// username: /mnt, /dev, ...
	Entries map[string][]*FSTab
}

// FindEntryByDevice checks fstab file for an entry using specified device
func (f *FSTabFile) FindEntryByDevice(device string) *FSTab {
	// final check, make sure we are not adding a duplicate
	for _, entries := range f.Entries {
		for _, entry := range entries {
			if entry.Device == device {
				return entry
			}
		}
	}
	return nil
}

// AddFSTabEntry places entries into fstab for durability
func (f *FSTabFile) AddFSTabEntry(mount *api.Mount) error {
	temp := &FSTab{
		Device:    mount.Device,
		Directory: mount.Path,
		Dump:      0,
		Fsck:      0,
	}
	switch mount.Fstype {
	case "ceph":
		temp.Type = "ceph"
		temp.Options = strings.Split(
			fmt.Sprintf("rw,relatime,name=%s,secret=%s,acl,_netdev",
				mount.KeyUser, mount.Key), ",")
	default:
		temp.Type = "ext4"
		temp.Options = []string{"defaults"}
	}
	if mount.Options != "" {
		temp.Options = strings.Split(mount.Options, ",")
	}

	linuxUser := mount.Username
	found := f.FindEntryByDevice(mount.Device)
	if found != nil {
		log.Warningf("Not adding: %v to fstab, already has: %v", temp, found)
		return nil
	}

	log.Infof("adding entry: %#v", temp)
	f.Entries[linuxUser] = append(f.Entries[linuxUser], temp)

	return nil
}

// FSTabParse parses fstab file
func (f *FSTabFile) FSTabParse() error {
	f.Entries = map[string][]*FSTab{}
	content, err := ioutil.ReadFile("/etc/fstab")
	if err != nil {
		return err
	}
	lines := strings.Split(string(content), "\n")
	for _, line := range lines {
		// empty line
		if len(line) == 0 {
			continue
		}
		// commented line... we throw these out, sorry!
		if string(line[0]) == "#" {
			continue
		}

		// break on whitespaces
		entry := strings.Fields(line)

		if len(entry) != 6 {
			log.Errorf("Invalid fstab entry: %v", entry)
		}

		temp := &FSTab{
			Device:    entry[0],
			Directory: entry[1],
			Type:      entry[2],
			Options:   strings.Split(entry[3], ","),
		}
		dump, err := strconv.ParseUint(entry[4], 10, 32)
		if err != nil {
			return err
		}
		fsck, err := strconv.ParseUint(entry[5], 10, 32)
		if err != nil {
			return err
		}
		temp.Dump = int(dump)
		temp.Fsck = int(fsck)
		// bit of a hack, but if the entry is already in fstab, the directory should exist
		f.Entries["root"] = append(f.Entries["root"], temp)
		log.Infof("entry: %#v", temp)
	}
	return nil
}

// FSTabWrite writes out the fstab file
func (f *FSTabFile) FSTabWrite() error {

	lines := ""
	for _, entries := range f.Entries {
		for _, entry := range entries {
			options := strings.Join(entry.Options, ",")
			lines += fmt.Sprintf("%s\t%s\t%s\t%s\t%d\t%d\n",
				entry.Device, entry.Directory, entry.Type, options, entry.Dump, entry.Fsck)
		}
	}

	err := ioutil.WriteFile("/etc/fstab", []byte(lines), 0644)
	if err != nil {
		return err
	}
	return nil
}

func lookup(path string) error {
	_, err := os.Stat(path)
	return err
}

func createPath(path, username string) error {

	user, err := GetPosixUser(username)
	if err != nil {
		return err
	}

	// attempt to create path
	err = os.MkdirAll(path, 0700)
	if err != nil {
		return err
	}

	// attempt to set linux perms
	err = os.Chown(path, user.Uid, user.Gid)
	if err != nil {
		return err
	}
	return nil
}

// RunMount runs the mount command to mount all items in fstab
func (f *FSTabFile) RunMount() error {

	// check that all directories exist
	for linuxUser, entries := range f.Entries {
		for _, entry := range entries {
			err := lookup(entry.Directory)
			// doesnt exist, so we should create it
			if os.IsNotExist(err) {
				createPath(entry.Directory, linuxUser)
			} else if err != nil {
				return err
			}
		}
	}

	cmd := exec.Command("mount", "-a")
	out, err := cmd.CombinedOutput()

	if err != nil {
		return ErrorEF("failed to run mount", err, log.Fields{"output": string(out)})
	}

	return nil
}
