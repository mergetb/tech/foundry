package forge

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/tredoe/osutil/user/crypt/sha512_crypt"

	api "gitlab.com/mergetb/tech/foundry/api"
)

func ForgeUsers(users []*api.User, use_systemd bool) error {

	addUser := AddUser
	addUserToGroup := AddUserToGroup
	if !use_systemd {
		addUser = AddUserNoSystemd
		addUserToGroup = AddUserToGroupNoSystemd
	}

	for _, user := range users {

		if user.Name == "" {
			return Error("username cannot be empty")
		}

		err := addUser(user.Name, user.Password)
		if err != nil {
			return err
		}

		if len(user.SshAuthorizedKeys) > 0 {
			AddSshAuthorizedKeys(user.Name, user.SshAuthorizedKeys)
		}

		log.Debugf("Adding user %s's private keys: %#v", user.Name, user.SshPrivateKeys)
		if len(user.SshPrivateKeys) > 0 {
			// first - remove whatever might be already added to restore clean slate
			if _, _, err = CleanupSshPrivateKeys(user.Name); err != nil {
				return err
			}
			if _, err = AddSshPrivateKeys(user.Name, user.SshPrivateKeys); err != nil {
				return err
			}
		}

		for _, group := range user.Groups {
			addUserToGroup(user.Name, group)
		}

	}

	return nil

}

func AddUser(name, passwd string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add user")

	// if user already exists, nothing to do
	_, err := user.Lookup(name)
	if err == nil {
		return nil
	}

	var cmd *exec.Cmd

	// check if the users own group already exists
	_, err = user.LookupGroup(name)
	if err != nil {

		if _, ok := err.(user.UnknownGroupError); ok {
			cmd = exec.Command(
				"useradd",
				"-m",              //create home directory
				"-s", "/bin/bash", // no one wants /bin/sh
				"-U", //create group with same name as user
			)
		} else {
			return ErrorEF("failed to query group", err, fields)
		}

	} else {

		cmd = exec.Command(
			"useradd",
			"-m",
			"-s", "/bin/bash",
			"-g", name,
		)

	}

	if passwd != "" {

		crypt := sha512_crypt.New()

		hash, err := crypt.Generate([]byte(passwd), nil)
		if err != nil {
			return ErrorEF("password hashing failure", err, fields)
		}
		cmd.Args = append(cmd.Args, "-p", string(hash))
	}
	cmd.Args = append(cmd.Args, name)
	out, err := cmd.CombinedOutput()
	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user", err, fields)
	}

	// delete the users password so they can set it if desired
	if passwd == "" {
		cmd = exec.Command("passwd", "-d", name)
		out, err = cmd.CombinedOutput()
		if err != nil {
			fields["out"] = string(out)
			return ErrorEF("failed to add user", err, fields)
		}
	}

	return nil

}

// strictly speaking, useradd isn't a systemd thing,
// but minimal linux distros (which are the ones without systemd)
// may not actually have useradd
func AddUserNoSystemd(name, passwd string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add user with adduser")

	// if user already exists, nothing to do
	_, err := user.Lookup(name)
	if err == nil {
		return nil
	}

	// check if the users own group already exists

	cmd := exec.Command(
		"adduser",
		"-D",
		"-s", "/bin/bash",
		name,
	)

	out, err := cmd.CombinedOutput()
	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user", err, fields)
	}

	cmd = exec.Command("passwd", "-d", name)
	out, err = cmd.CombinedOutput()
	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to set empty password for user", err, fields)
	}

	cmd = exec.Command("passwd", "-u", name)
	out, err = cmd.CombinedOutput()
	// ignore unlock errors

	if passwd != "" {
		log.Warn("adding passwords not supported without systemd")
	}

	return nil
}

func AddSshAuthorizedKeys(user string, keys []string) error {

	u, err := GetPosixUser(user)
	if err != nil {
		return err
	}

	fields := log.Fields{"user": user}
	sshdir := fmt.Sprintf("%s/.ssh", u.HomeDir)
	authfile := fmt.Sprintf("%s/authorized_keys", sshdir)
	fields["path"] = sshdir
	fields["authfile"] = authfile

	log.WithFields(fields).Info("add-ssh-key")

	// ensure the users .ssh dir exists
	err = os.MkdirAll(sshdir, 0700)
	if err != nil {
		return ErrorEF("failed to ensure users .ssh dir", err, fields)
	}
	// ensure ownership
	err = os.Chown(sshdir, u.Uid, u.Gid)
	if err != nil {
		return ErrorEF("failed to own authorized_keys file", err, fields)
	}

	// open the auth'd keys file
	f, err := os.OpenFile(authfile, os.O_CREATE|os.O_RDONLY, 0600)
	if err != nil {
		return ErrorEF("failed to open users authorized_keys", err, fields)
	}

	// keep a de-duplicating list of keys
	km := make(map[string]byte)

	// scan the contents and put into map
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		km[scanner.Text()] = 0
	}
	f.Close()
	err = scanner.Err()
	if err != nil {
		return ErrorEF("failed to read authorized_keys", err, fields)
	}

	// integrate the provided keys
	for _, key := range keys {
		// key normally ends in newlne, but scanner above removed them
		km[strings.TrimRight(key, "\r\n")] = 0
	}

	// fabricate result
	var result string
	for key := range km {
		result += fmt.Sprintln(key)
	}

	// write back to file
	err = ioutil.WriteFile(authfile, []byte(result), 0600)
	if err != nil {
		return ErrorEF("failed to write update authorized_keys file", err, fields)
	}

	// ensure ownership
	err = os.Chown(authfile, u.Uid, u.Gid)
	if err != nil {
		return ErrorEF("failed to own authorized_keys file", err, fields)
	}

	return nil

}

// AddSshPrivateKeys adds private `keys` to `user`'s .ssh configuration
// first it saves them under ~user/.ssh/id_mrg-<N>, then updates
// ~user/.ssh/config to have them read whenever users runs ssh
// we assume N is constant for a particular materialization
func AddSshPrivateKeys(user string, keys []string) ([]string, error) {

	fields := log.Fields{"user": user}
	log.WithFields(fields).Infof("add-private-ssh-key")

	u, err := GetPosixUser(user)
	if err != nil {
		return nil, err
	}

	sshdir := fmt.Sprintf("%s/.ssh", u.HomeDir)
	fields["path"] = sshdir
	// ensure the users .ssh dir exists
	err = os.MkdirAll(sshdir, 0700)
	if err != nil {
		return nil, ErrorEF("failed to ensure users .ssh dir", err, fields)
	}
	// ensure ownership
	err = os.Chown(sshdir, u.Uid, u.Gid)
	if err != nil {
		return nil, ErrorEF("failed to own .ssh dir", err, fields)
	}

	// identity files (private keys) installed here will have special names id_mrg-<N>,
	// where N is integer, starting from 0;
	// we assume N is constant for a particular materialization
	// later we'll add them as identities to .ssh/config
	var identities []string
	for i, key := range keys {
		id_base := fmt.Sprintf("id_mrg-%d", i)
		id_file := fmt.Sprintf("%s/%s", sshdir, id_base)
		fields["identity"] = id_file

		f, err := os.OpenFile(id_file, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
		if err != nil {
			return identities, ErrorEF("failed to create unique .ssh/id_mrg- file", err, fields)
		}
		defer f.Close()

		log.WithFields(fields).Info("add-private-ssh-key")
		if err := f.Chmod(0600); err != nil {
			return identities, ErrorEF("failed to chmod private key", err, fields)
		}
		// ensure ownership
		if err = os.Chown(id_file, u.Uid, u.Gid); err != nil {
			return identities, ErrorEF("failed to own private key", err, fields)
		}
		if _, err := f.Write([]byte(key)); err != nil {
			return identities, ErrorEF("failed to write private key", err, fields)
		}
		identities = append(identities, fmt.Sprintf("IdentityFile ~/.ssh/%s", id_base))
	}
	// add identities to .ssh/config
	ssh_config := fmt.Sprintf("%s/config", sshdir)
	f, err := os.OpenFile(ssh_config, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	defer f.Close()

	if err != nil {
		return identities, ErrorEF("failed to append to .ssh/config", err, fields)
	}
	if _, err := f.Write([]byte(strings.Join(identities, "\n") + "\n")); err != nil {
		return identities, ErrorEF("failed to append identities to .ssh/config", err, fields)
	}
	if err = os.Chown(ssh_config, u.Uid, u.Gid); err != nil {
		return identities, ErrorEF("failed to own .ssh/config", err, fields)
	}


	return identities, nil
}

// CleanupSshPrivateKeys: remove private keys installed by AddSshPrivateKeys
func CleanupSshPrivateKeys(user string) ([]string, []string, error) {
	u, err := GetPosixUser(user)
	if err != nil {
		return nil, nil, err
	}
	fields := log.Fields{"user": user}
	log.WithFields(fields).Infof("cleanup-private-ssh-key")

	sshdir := fmt.Sprintf("%s/.ssh", u.HomeDir)
	// delete key files
	identities, err := filepath.Glob(fmt.Sprintf("%s/id_mrg-[0-9]*", sshdir))
	if err != nil {
		return nil, nil, err
	}
	for _, f := range identities {
		if err = os.Remove(f); err != nil {
			return nil, nil, err
		}
	}
	// cleanup .ssh/config
	var removed_lines []string
	sshconf := fmt.Sprintf("%s/config", sshdir)

	if _, err = os.Stat(sshconf); os.IsNotExist(err) {
		return identities, nil, nil
	}
	f, err := os.OpenFile(sshconf, os.O_CREATE|os.O_RDONLY, 0600)
	if err != nil {
		return identities, nil, ErrorEF("failed to open user's .ssh/config", err, fields)
	}

	config_text := ""
	scanner := bufio.NewScanner(f)
	prefix := "IdentityFile ~/.ssh/id_mrg-"
	for scanner.Scan() {
		if line := scanner.Text(); strings.HasPrefix(line, prefix) {
			removed_lines = append(removed_lines, line)
		} else {
			config_text += line + "\n"
		}
	}
	f.Close()
	if err = scanner.Err(); err != nil {
		return identities, nil, ErrorEF("failed to read user's .ssh/config", err, fields)
	}

	if len(removed_lines) != 0 {
		// write back the result
		err = ioutil.WriteFile(sshconf, []byte(config_text), 0600)
		if err != nil {
			return identities, removed_lines, ErrorEF("failed to write cleaned up user's .ssh/config file", err, fields)
		}
	}

	return identities, removed_lines, nil
}

type PosixUser struct {
	Uid      int
	Gid      int
	Username string
	Name     string
	HomeDir  string
}

func GetPosixUser(name string) (*PosixUser, error) {

	fields := log.Fields{"user": name}

	u, err := user.Lookup(name)
	if err != nil {
		return nil, ErrorEF("could not lookup user", err, fields)
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return nil, ErrorEF("failed to convert uid", err, fields)
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return nil, ErrorEF("failed to convert gid", err, fields)
	}

	return &PosixUser{
		Uid:      uid,
		Gid:      gid,
		Username: name,
		Name:     u.Name,
		HomeDir:  u.HomeDir,
	}, nil

}

func AddUserToGroup(name, group string) error {

	fields := log.Fields{
		"name":  name,
		"group": group,
	}
	log.WithFields(fields).Info("add user to group")

	// ensure the group exists
	_, err := user.LookupGroup(group)
	if err != nil {
		err = AddGroup(group)
		if err != nil {
			return err
		}
	}

	cmd := exec.Command("usermod", "-aG", group, name)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user to group", err, fields)
	}

	return nil

}

// strictly speaking, groupadd isn't a systemd thing,
// but minimal linux distros (which are the ones without systemd)
// may not actually have groupadd
func AddUserToGroupNoSystemd(name, group string) error {

	fields := log.Fields{
		"name":  name,
		"group": group,
	}
	log.WithFields(fields).Info("add user to group")

	// ensure the group exists
	_, err := user.LookupGroup(group)
	if err != nil {
		err = AddGroupNoSystemd(group)
		if err != nil {
			return err
		}
	}

	cmd := exec.Command("addgroup", name, group)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user to group", err, fields)
	}

	return nil

}

func AddGroup(name string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add group")

	cmd := exec.Command("groupadd", name)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add group", err, fields)
	}

	return nil

}

func AddGroupNoSystemd(name string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add group with addgroup")

	cmd := exec.Command("addgroup", name)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add group", err, fields)
	}

	return nil
}
