package forge

import (
	"log"

	"github.com/mergetb/embiggen-disk"
)

func ForgeRootfs(path string) error {

	log.Printf("expanding %s", path)

	return embiggen.Embiggen(path)

}
