package forge

import (
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
	"os/exec"
)

// Forge is the top level function responsible for implementing "forging" a set
// of foundry instructions onto a node
func Forge(mc *api.MachineConfig, use_systemd bool, embiggen_path, grub_cfg string) error {

	log.Debugf("config: %+v", *mc)

	log.WithFields(log.Fields{"id": mc.Id}).Info("forge")

	if mc.Config.Hostname != "" {
		err := ForgeHostname(mc.Config.Hostname, use_systemd)
		if err != nil {
			return err
		}
	}

	if mc.Config.Hosts != nil {
		err := ForgeHosts(mc.Config.Hosts)
		if err != nil {
			return err
		}
	}

	err := ForgeInterfaces(mc.Config.Interfaces, use_systemd)
	if err != nil {
		return err
	}

	err = ForgeRoutes(use_systemd)
	if err != nil {
		return err
	}

	err = ForgeStaticRoutes(mc.Config.Routes, mc.Config.Interfaces, use_systemd)
	if err != nil {
		return err
	}

	err = ForgeUsers(mc.Config.Users, use_systemd)
	if err != nil {
		return err
	}

	if use_systemd {
		err = RestartNetworkd()
		if err != nil {
			return err
		}
	}

	if mc.Config.ExpandRootfs {
		err := ForgeRootfs(embiggen_path)
		if err != nil {
			return err
		}
	}

	err = AddMounts(mc.Config.Mounts)
	if err != nil {
		return err
	}

	err = ForgeCmdline(grub_cfg)
	if err != nil {
		return err
	}

	return nil

}

func RestartNetworkd() error {

	// Restart networkd after adding routes and interfaces.
	cmd := exec.Command("service", "systemd-networkd", "restart")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return ErrorEF("restarting networkd failed", err, log.Fields{
			"out": string(out),
		})
	}

	return nil

}
