package foundry

import (
	"fmt"
)

const (
	MachineConfigKeyPrefix = "/mc"
)

func (m *MachineConfig) Key() string {
	return fmt.Sprintf("%s/%s", MachineConfigKeyPrefix, m.Id)
}
func (m *MachineConfig) GetVersion() int64  { return m.Ver }
func (m *MachineConfig) SetVersion(v int64) { m.Ver = v }
func (m *MachineConfig) Value() interface{} { return m }
