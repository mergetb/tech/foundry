#!/bin/bash

set -e

docker build -f debian/builder.dock -t foundry-builder .
docker run -v `pwd`:/foundry foundry-builder /foundry/build-deb.sh
