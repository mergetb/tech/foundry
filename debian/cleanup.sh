#!/bin/bash

sudo rm -rf *.debhelper \
	*.substvars \
	debhelper* \
	foundryc \
	foundryd \
	foundryctl \
	tmp \
	files \
