
let name = "foundry_"+Math.random().toString().substr(-6);

topo = {
  name: name,
  nodes: [ deb('client'), deb('server') ],
  links: [ Link('client', 0, 'server', 0, { mac: { 'client': '00:00:00:00:00:47' } } ) ]
}

function deb(name) {
  return {
    name: name,
    image: 'debian-bullseye',
    memory: { capacity: GB(4) },
    proc: { cores: 4 },
    mounts: [
      {source: env.PWD+'/..', point: '/tmp/foundry'}
    ],
    disks: [
      { size: "1G", dev: 'vdb', bus: 'virtio' },
   ],
  }
}
